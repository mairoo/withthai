# -*- coding:utf-8 -*-

from xml.etree.ElementTree import parse

tree = parse('telex-utf8.xml')

f = open('nectec-telex.csv', 'w', encoding='utf-8')

docs = tree.findall('Doc')

for d in docs:
    word = d.findtext('tsearch')

    if word is not None:
        print(word)
        f.write('%s\n' % word)

f.close()
