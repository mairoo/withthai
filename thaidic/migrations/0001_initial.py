# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-30 17:29
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Date created')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Date updated')),
                ('title', models.CharField(max_length=250, unique=True, verbose_name='Title')),
            ],
            options={
                'verbose_name': 'entry',
                'verbose_name_plural': 'entries',
                'ordering': ['title'],
            },
        ),
        migrations.CreateModel(
            name='Meaning',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Date created')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Date updated')),
                ('part_of_speach', models.IntegerField(choices=[(0, 'Noun'), (1, 'Pronoun'), (2, 'Verb'), (3, 'Adverb'), (4, 'Preposition'), (5, 'Conjunction'), (6, 'Interjection')], default=0, verbose_name='Category')),
                ('description', models.CharField(max_length=512, verbose_name='Description')),
                ('position', models.IntegerField(default=1, verbose_name='Position')),
                ('entry', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='entry_meaning_set', to='thaidic.Entry', verbose_name='Entry')),
            ],
            options={
                'verbose_name': 'meaning',
                'verbose_name_plural': 'meanings',
                'ordering': ['position'],
            },
        ),
        migrations.CreateModel(
            name='Pronunciation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Date created')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Date updated')),
                ('title', models.CharField(max_length=250, verbose_name='Pronunciation')),
                ('position', models.IntegerField(default=1, verbose_name='Position')),
                ('entry', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='entry_pronunciation_set', to='thaidic.Entry', verbose_name='Entry')),
            ],
            options={
                'verbose_name': 'pronunciation',
                'verbose_name_plural': 'pronunciations',
                'ordering': ['position'],
            },
        ),
        migrations.CreateModel(
            name='Sentence',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Date created')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Date updated')),
                ('title', models.CharField(max_length=512, verbose_name='Sentence')),
                ('position', models.IntegerField(default=1, verbose_name='Position')),
                ('meaning', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='meaning_sentence_set', to='thaidic.Meaning', verbose_name='Meaning')),
            ],
            options={
                'verbose_name': 'sentence',
                'verbose_name_plural': 'sentences',
                'ordering': ['position'],
            },
        ),
    ]
