from django.contrib import admin

from .models import (
    Entry, Pronunciation, Meaning, Sentence
)


class PronunciationInline(admin.StackedInline):
    model = Pronunciation
    extra = 1


class MeaningInline(admin.StackedInline):
    model = Meaning
    extra = 1


class SentenceInline(admin.StackedInline):
    model = Sentence
    extra = 1


class EntryAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)
    ordering = ['title', ]
    inlines = [PronunciationInline, MeaningInline]


class MeaningAdmin(admin.ModelAdmin):
    list_display = ('entry', 'part_of_speach',)
    inlines = [SentenceInline, ]


admin.site.register(Entry, EntryAdmin)
admin.site.register(Meaning, MeaningAdmin)
