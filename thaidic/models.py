from django.db import models
from django.utils.translation import ugettext_lazy as _

from model_utils.models import TimeStampedModel


class Entry(TimeStampedModel):
    title = models.CharField(
        verbose_name=_('Title'),
        max_length=250,
        unique=True
    )

    class Meta:
        verbose_name = _('entry')
        verbose_name_plural = _('entries')
        ordering = ['title']

    def __str__(self):
        return self.title


class Pronunciation(TimeStampedModel):
    title = models.CharField(
        verbose_name=_('Pronunciation'),
        max_length=250
    )

    entry = models.ForeignKey(
        Entry,
        verbose_name=_('Entry'),
        related_name='entry_pronunciation_set'
    )

    position = models.IntegerField(
        verbose_name=_('Position'),
        default=1,
    )

    class Meta:
        verbose_name = _('pronunciation')
        verbose_name_plural = _('pronunciations')
        ordering = ['position']

    def __str__(self):
        return self.title


class Meaning(TimeStampedModel):
    NOUN = 0
    PRONOUN = 1
    VERB = 2
    ADVERB = 3
    PREPOSITION = 4
    CONJUNCTION = 5
    INTERJECTION = 6

    entry = models.ForeignKey(
        Entry,
        verbose_name=_('Entry'),
        related_name='entry_meaning_set'
    )

    PART_OF_SPEECH_CHOICES = (
        (NOUN, _('Noun')),
        (PRONOUN, _('Pronoun')),
        (VERB, _('Verb')),
        (ADVERB, _('Adverb')),
        (PREPOSITION, _('Preposition')),
        (CONJUNCTION, _('Conjunction')),
        (INTERJECTION, _('Interjection')),
    )

    part_of_speach = models.IntegerField(
        verbose_name=_('Category'),
        choices=PART_OF_SPEECH_CHOICES,
        default=NOUN
    )

    description = models.CharField(
        verbose_name=_('Description'),
        max_length=512
    )

    position = models.IntegerField(
        verbose_name=_('Position'),
        default=1,
    )

    class Meta:
        verbose_name = _('meaning')
        verbose_name_plural = _('meanings')
        ordering = ['position']

    def __str__(self):
        return self.description


class Sentence(TimeStampedModel):
    title = models.CharField(
        verbose_name=_('Sentence'),
        max_length=512
    )

    meaning = models.ForeignKey(
        Meaning,
        verbose_name=_('Meaning'),
        related_name='meaning_sentence_set'
    )

    position = models.IntegerField(
        verbose_name=_('Position'),
        default=1,
    )

    class Meta:
        verbose_name = _('sentence')
        verbose_name_plural = _('sentences')
        ordering = ['position']

    def __str__(self):
        return self.title
